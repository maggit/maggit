maggit.db.io package
====================

.. automodule:: maggit.db.io

maggit.db.io.loose module
-------------------------

.. automodule:: maggit.db.io.loose
    :members:
    :show-inheritance:

maggit.db.io.pack module
------------------------

.. automodule:: maggit.db.io.pack
    :members:

maggit.db.io.packedref module
-----------------------------

.. automodule:: maggit.db.io.packedref
    :members:
    :undoc-members:
    :show-inheritance:
