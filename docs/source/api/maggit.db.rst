maggit.db package
=================

.. automodule:: maggit.db

.. toctree::
    maggit.db.io

maggit.db.db module
-------------------

.. automodule:: maggit.db.db
    :members:
    :undoc-members:
    :show-inheritance:

maggit.db.repo module
---------------------

.. automodule:: maggit.db.repo
    :members:
    :undoc-members:
    :inherited-members:
