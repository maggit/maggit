Installing
==========

Installing with pypi
--------------------

Maggit is available on pypi. Just use it ::

    $ pip install maggit


Installing from source
----------------------

You can directly download sources and install from them ::

    $ git clone https://gitlab.com/maggit/maggit.git
    $ cd maggit
    $ python3 setup.py install .

To test that everything is ok ::

    $ pip install pytest
    $ py.test


You are now ready to use Maggit.
Read the :doc:`tutorial` if you don't know how.
