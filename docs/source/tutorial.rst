Tutorial
========

Importing Maggit
----------------

This is pretty simple ::

    import maggit

You can use the star import if you want ::

    from maggit import *

For the rest of the tutorial, we'll assume that you import maggit and do not
use the star import.

Get a repository
----------------

A :class:`~maggit.Repo` is one the central objects when interactig with maggit::
    
    # Create a repository
    repo = maggit.Repo()

By default, Maggit will look for a git repository in your current directory.
If you want to explore a repository elsewhere, specify it::

    repo = maggit.Repo("a/git/repository")

You don't have to specify a the root directory of the repository.
If the directory you specify is not a git repository, maggit will look up in
the parents directories.

References
----------

Once we've got a repository, the first thing we may want to do is list it's
references (branches, tags)::

    branches = repo.branches
    print(repo.branches.keys())

`repo.branches` is a dictionnary mapping all branches of the repository

If we want manipulate the `master` branch, lets get it::

    master = branches['master']

In the same way `repo.tags` is a dictionnary mapping all tags of the repository

All the references we get, whatever they are branches, lightweigh tag or real
tag objects share a common api.
The most interesting here, is to get the commit object pointed to by the ref::

    commit = master.commit

Print the log of the 10th last commit of the master branch
----------------------------------------------------------

We just have to go through the parent of the commit and print the message each
time ::

    commit = master.commit
    for i in range(10):
        print(commit.message)
        commit = commit.parents[0]

Print the content of a file
---------------------------

There are several way of doing it.

The low level way is the following::

    current = commit.tree
    for part in path.split(b'/'):
        current = current.entries[part]
    blob = current
    print(blob.content)

But you can also use a high level api ::

    entry = maggit.Entry(commit, path)
    # the entry is a intermediate object making the link between a gitobject
    # and a particular commit
    blob = entry.gitObject
    print(blob.content)

The commit object
-----------------

Other important object in Maggit is naturally the git objects.
A git object firstly allow us to access to the content of the object itself.

The commit object is one of them. Naturally it allows us to access the commit attributes::

    commit.message
    commit.first_line #The first line of the message
    commit.author
    commit.committer
    commit.parents # The parents of the commit
    commit.tree # The tree object

