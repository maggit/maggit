# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db.io import config
import io
from subprocess import check_output
from pathlib import Path

emptys = [ b'',
           b'\xef\xbb\xbf',
           b'\n',
           b'\xef\xbb\xbf\n',
           b'\n\n\n',
           b'# a comment\n',
           b'; a comment\n',
           b'\n# a comment\n',
           b'\xef\xbb\xbf\n# a comment\n',
         ]
@pytest.fixture(params=emptys)
def empty(request):
    return io.BytesIO(request.param)

def test_empty(empty):
    assert config.read_file(empty)[0] == []

emptySections = [  b'[section]',
            b'[section]\n',
            b'[section] # a comment\n',
            b'[section] \n # a comment'
          ]
@pytest.fixture(params=emptySections)
def emptySection(request):
    return io.BytesIO(request.param)

def test_emptySection(emptySection):
    assert config.read_file(emptySection)[0] == [((b'section', None), [])]


boolsEmpty = [ b'[section] name', ]
@pytest.fixture(params=boolsEmpty)
def boolEmpty(request):
    return io.BytesIO(request.param)

def test_boolEmpty(boolEmpty):
    assert config.read_file(boolEmpty)[0] == [ ((b'section', None), [(b'name', None)] )
                                             ]

boolsTrue = [  b'[section] name=true',
               b'[section] name=TRUE',
               b'[section] name=True',
               b'[section] name=truE',
               b'[section] name=1',
               b'[section] name=yes',
               b'[section] name=Yes',
               b'[section] name=YeS',
               b'[section] name=on',
               b'[section] name=oN',
               b'[section] name=On',
               b'[section] # comment\nname=true ;another'
             ]
@pytest.fixture(params=boolsTrue)
def boolTrue(request):
    return io.BytesIO(request.param)

def test_boolTrue(boolTrue):
    print(boolTrue.getvalue())
    assert config.read_file(boolTrue)[0] == [ ((b'section', None), [(b'name', True)] )
                                            ]

boolsFalse = [ b'[section] name=false',
               b'[section] name=False',
               b'[section] name=faLse',
               b'[section] name=FaLse',
               b'[section] name=0',
               b'[section] name=no',
               b'[section] name=No',
               b'[section] name=NO',
               b'[section] name=off',
               b'[section] name=oFf',
               b'[section] name=oFF',
             ]
@pytest.fixture(params=boolsFalse)
def boolFalse(request):
    return io.BytesIO(request.param)

def test_boolFalse(boolFalse):
    print(boolFalse.getvalue())
    assert config.read_file(boolFalse)[0] == [ ((b'section', None), [(b'name', False)] )
                                             ]


nums10 = [ b'[section] name=10',
           b'; comment\n[section] name=10',
           b'[section]\nname=10',
           b'[section]#u;\nname=10',
           b'[section]\nname=10#comment',
           b'[section]\n#comment\nname=10#comment',
         ]
@pytest.fixture(params=nums10)
def num10(request):
    return io.BytesIO(request.param)

def test_num10(num10):
    print(num10.getvalue())
    assert config.read_file(num10)[0] == [ ((b'section', None), [(b'name', 10)] )
                                         ]


values = [ b'[section] name=value',
           b'; comment\n[section] name=value',
           b'[section]\nname=value',
           b'[section]#u;\nname=value',
           b'[section]\nname=value#comment',
           b'[section]\n#comment\nname=value#comment',
         ]
@pytest.fixture(params=values)
def value(request):
    return io.BytesIO(request.param)

def test_value(value):
    print(value.getvalue())
    assert config.read_file(value)[0] == [ ((b'section', None), [(b'name', b'value')] )
                                         ]

subsections = [ b'[section "subsection"] name=value',
                b'; comment\n[section   "subsection"] name=value',
                b'[section \t "subsection"]\nname=value',
                b'[ section "subsection"]#u;\nname=value',
                b'[section "subsection" ]\nname=value#comment',
                b'[ section "subsection" ]\n#comment\nname=value#comment',
              ]
@pytest.fixture(params=subsections)
def subsection(request):
    return io.BytesIO(request.param)

def test_subsection(subsection):
    print(subsection.getvalue())
    assert config.read_file(subsection)[0] == [ ((b'section', b'subsection'), [(b'name', b'value')] )
                                              ]


subsections1 = [ b'[section "subs\\"ection"] name=value',
                b'; comment\n[section   "subs\\"ection"] name=value'
              ]
@pytest.fixture(params=subsections1)
def subsection1(request):
    return io.BytesIO(request.param)

def test_subsection1(subsection1):
    print(subsection1.getvalue())
    assert config.read_file(subsection1)[0] == [ ((b'section', b'subs"ection'), [(b'name', b'value')])]


quoteds = [ (b'[section "subsection"] name="value"', b'value'),
            (b'[section "subsection"] name="value  "', b'value  '),
            (b'[section "subsection"] name="   value  "', b'   value  '),
            (b'[section "subsection"] name="value" for test', b'value for test'),
            (b'[section "subsection"] name="value  " for test', b'value   for test'),
            (b'[section "subsection"] name=  "value" for test', b'value for test'),
            (b'[section "subsection"] name=  "value" for test   ', b'value for test'),
            (b'[section "subsection"] name=  "value\" for test   ', b'value for test'),
            (b'[section "subsection"] name=  "value" for \\\ntest   ', b'value for \ntest'),
              ]
@pytest.fixture(params=quoteds)
def quoted(request):
    return io.BytesIO(request.param[0]), request.param[1]

def test_quoted(quoted):
    stream, result = quoted
    print(stream.getvalue())
    assert config.read_file(stream)[0] == [((b'section', b'subsection'), [(b'name', result)])]



full_config = b"""#
# This is the config file, and
# a '#' or ';' character indicates
# a comment
#

; core variables
[core]
       ; Don't trust file modes
       filemode = false

; Our diff algorithm
[diff]
       external = /usr/local/bin/diff-wrapper
       renames = true

; Proxy settings
[core]
       gitproxy=proxy-command for kernel.org
       gitproxy=default-proxy ; for all the rest

; HTTP
[http]
       sslVerify
[http "https://weak.example.com"]
       sslVerify = false
       cookieFile = /tmp/cookie.txt"""

def test_full_config():
    stream = io.BytesIO(full_config)
    assert config.read_file(stream)[0] == [ ((b'core', None), [(b'filemode', False)]),
                                            ((b'diff', None), [(b'external', b'/usr/local/bin/diff-wrapper'),
                                                               (b'renames', True)]),
                                            ((b'core', None), [(b'gitproxy', b'proxy-command for kernel.org'),
                                                               (b'gitproxy', b'default-proxy')]),
                                            ((b'http', None), [(b'sslverify', None)]),
                                            ((b'http', b'https://weak.example.com'), [(b'sslverify', False),
                                                                                      (b'cookiefile', b'/tmp/cookie.txt')])
                                          ]


with_include = b"""# Core variables
[core]
       ; Don't trust file modes
       filemode = false

# Our diff algorithm
[diff]
       external = /usr/local/bin/diff-wrapper
       renames = true

[branch "devel"]
       remote = origin
       merge = refs/heads/devel

# Proxy settings
[core]
       gitProxy="ssh" for "kernel.org"
       gitProxy=default-proxy ; for the rest

[include]
       path = /path/to/foo.inc ; include by absolute path
       path = foo ; expand "foo" relative to the current file
       path = ~/foo ; expand "foo" in your $HOME directory"""

def test_with_include():
    stream = io.BytesIO(with_include)
    assert config.read_file(stream)[0] == [ ((b'core', None), [(b'filemode', False)]),
                                            ((b'diff', None), [(b'external', b'/usr/local/bin/diff-wrapper'),
                                                               (b'renames', True)]),
                                            ((b'branch', b"devel"), [(b'remote', b"origin"),
                                                                     (b'merge', b"refs/heads/devel")]),
                                            ((b'core', None), [(b'gitproxy', b'ssh for kernel.org'),
                                                               (b'gitproxy', b'default-proxy')]),
                                            ((b'include', None), [(b'path', b'/path/to/foo.inc'),
                                                                  (b'path', b'foo'),
                                                                  (b'path', b'~/foo')])
                                       ]

sample0 = br'''[user]
	name = Pavan Kumar Sunkara
	email = pavan.sss1991@gmail.com
[core]
	editor = vim
	whitespace = fix,-indent-with-non-tab,trailing-space,cr-at-eol
	excludesfile = ~/.gitignore
[sendemail]
	smtpencryption = tls
	smtpserver = smtp.gmail.com
	smtpuser = pavan.sss1991@gmail.com
	smtppass = password
	smtpserverport = 587
[web]
	browser = google-chrome
[instaweb]
	httpd = apache2 -f
[rerere]
	enabled = 1
	autoupdate = 1
[push]
	default = matching
[color]
	ui = auto
[color "branch"]
	current = yellow bold
	local = green bold
	remote = cyan bold
[color "diff"]
	meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
	whitespace = red reverse
[color "status"]
	added = green bold
	changed = yellow bold
	untracked = red bold
[diff]
	tool = vimdiff
[difftool]
	prompt = false
[github]
	user = pkumar
	token = token
[gitflow "prefix"]
	feature = feature-
	release = release-
	hotfix = hotfix-
	support = support-
	versiontag = v
[alias]
	a = add --all
	ai = add -i
	#############
	ap = apply
	as = apply --stat
	ac = apply --check
	#############
	ama = am --abort
	amr = am --resolved
	ams = am --skip
	#############
	b = branch
	ba = branch -a
	bd = branch -d
	br = branch -r
	#############
	c = commit
	ca = commit -a
	cm = commit -m
	cem = commit --allow-empty -m
	cam = commit -am
	cd = commit --amend
	cad = commit -a --amend
	ced = commit --allow-empty --amend
	#############
	d = diff
	dc = diff --cached
	dl = difftool
	dlc = difftool --cached
	dk = diff --check
	dp = diff --patience
	dck = diff --cached --check
	#############
	f = fetch
	fo = fetch origin
	fu = fetch upstream
	#############
	fp = format-patch
	#############
	fk = fsck
	#############
	g = grep -p
	#############
	l = log --oneline
	lg = log --oneline --graph --decorate
	#############
	ls = ls-files
	lsf = "!git ls-files | grep -i"
	#############
	m = merge
	ma = merge --abort
	mc = merge --continue
	ms = merge --skip
	#############
	o = checkout
	ob = checkout -b
	#############
	pr = prune -v
	#############
	ps = push
	psf = push -f
	psu = push -u
	pso = push origin
	psao = push --all origin
	psfo = push -f origin
	psuo = push -u origin
	psom = push origin master
	psfom = push -f origin master
	psuom = push -u origin master
	#############
	pl = pull
	plu = pull -u
	plo = pull origin
	plp = pull upstream
	plom = pull origin master
	plpm = pull upstream master
	#############
	pb = pull --rebase
	pbo = pull --rebase origin
	pbp = pull --rebase upstream
	pbom = pull --rebase origin master
	pbpm = pull --rebase upstream master
	#############
	rb = rebase
	rba = rebase --abort
	rbc = rebase --continue
	rbi = rebase --interactive
	rbs = rebase --skip
	#############
	re = reset
	rh = reset HEAD
	reh = reset --hard
	rem = reset --mixed
	res = reset --soft
	rehh = reset --hard HEAD
	remh = reset --mixed HEAD
	resh = reset --soft HEAD
	#############
	r = remote
	ra = remote add
	rr = remote rm
	rv = remote -v
	rm = remote rename
	rp = remote prune
	rs = remote show
	rao = remote add origin
	rau = remote add upstream
	rso = remote show origin
	rsu = remote show upstream
	rpo = remote prune origin
	rpu = remote prune upstream
	#############
	s = status
	sb = status -s -b
	#############
	sa = stash apply
	sc = stash clear
	sd = stash drop
	sl = stash list
	sp = stash pop
	ss = stash save
	sw = stash show
	#############
	w = show
	wp = show -p
	wr = show -p --no-color
	#############
	svnr = svn rebase
	svnd = svn dcommit
	svnl = svn log --oneline --show-commit
	#############
	assume = update-index --assume-unchanged
	unassume = update-index --no-assume-unchanged
	assumed = "!git ls-files -v | grep ^h | cut -c 3-"
	unassumeall = !git assumed | xargs git update-index --no-assume-unchanged
	assumeall = "!git st -s | awk {'print $2'} | xargs git assume"
	#############
	ours = "!f() { git checkout --ours $@ && git add $@; }; f"
	theirs = "!f() { git checkout --theirs $@ && git add $@; }; f"
	#############
	whois = "!sh -c 'git log -i -1 --pretty=\"format:%an <%ae>\n\" --author=\"$1\"' -"
	whatis = show -s --pretty='tformat:%h (%s, %ad)' --date=short
	#############
	barebranch = !sh -c 'git symbolic-ref HEAD refs/heads/$1 && git rm --cached -r . && git clean -xfd' -
	flat = clone --depth 1
	subpull = !git submodule foreach git pull --tags origin master
	subrepo = !sh -c 'filter-branch --prune-empty --subdirectory-filter $1 master' -
	human = name-rev --name-only --refs=refs/heads/*
	serve = !git daemon --reuseaddr --verbose  --base-path=. --export-all ./.git
	snapshot = !git stash save "snapshot: $(date)" && git stash apply "stash@{0}"'''

sample1 = b'''[user]
	name = Pavan Kumar Sunkara
	email = pavan.sss1991@gmail.com
[core]
	editor = vim
	whitespace = fix,-indent-with-non-tab,trailing-space,cr-at-eol
[sendemail]
	smtpencryption = tls
	smtpserver = smtp.gmail.com
	smtpuser = pavan.sss1991@gmail.com
	smtppass = password
	smtpserverport = 587
	to = git@vger.kernel.org
[web]
	browser = google-chrome
[rerere]
	enabled = 1
	autoupdate = 1
[alias]
	s = status
	sb = status -s -b
	#############
	sa = stash apply
	sc = stash clear
	sd = stash drop
	sl = stash list
	sp = stash pop
	ss = stash save
	#############
	d = diff
	dc = diff --cached
	dk = diff --check
	dck = diff --cached --check
	#############
	c = commit
	ca = commit -a
	cm = commit -m
	cam = commit -am
	cd = commit --amend
	cad = commit -a --amend
	#############
	a = add .
	#############
	ap = apply
	as = apply --stat
	ac = apply --check
	#############
	l = log --oneline
	lg = log --oneline --graph --decorate
	#############
	o = checkout
	ob = checkout -b
	#############
	b = branch
	ba = branch -a
	bd = branch -d
	br = branch -r
	#############
	f = fetch
	fo = fetch origin
	#############
	fp = format-patch
	#############
	fk = fsck
	#############
	m = merge
	#############
	pr = prune -v
	#############
	ps = push
	pl = pull
	pb = pull --rebase
	psf = push -f
	psu = push -u
	plu = pull -u
	pso = push origin
	plo = pull origin
	pbo = pull --rebase origin
	psfo = push -f origin
	psuo = push -u origin
	pluo = pull -u origin
	#############
	rb = rebase
	#############
	re = reset
	rh = reset HEAD
	reh = reset --hard
	rem = reset --mixed
	res = reset --soft
	rehh = reset --hard HEAD
	remh = reset --mixed HEAD
	resh = reset --soft HEAD
	#############
	r = remote
	ra = remote add
	rm = remote rm
	rv = remote -v
	rp = remote prune
	rs = remote show
	rso = remote show origin
	rpo = remote prune origin
	#############
	w = show
	#############
	meldon = config diff.external /home/pkumar/.meld.py
	meldof = config --remove-section diff
	#############
	human = name-rev --name-only --refs=refs/heads/*
[color]
	ui = true
[color "branch"]
	current = yellow bold
	local = green bold
	remote = cyan bold
[color "diff"]
	meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
	whitespace = red reverse
[color "status"]
	added = green bold
	changed = yellow bold
	untracked = red bold
[github]
	user = pkumar
	token = token'''


sample2 = b'''[user] 
name = John Smith
email = john@example.com
[alias]
st = status
co = checkout
br = branch
up = rebase
ci = commit
[core]
editor = vim'''

sample3 = b'''[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "origin"]
    url = git://repohost/project1.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
    remote = origin
    merge = refs/heads/master'''

sample4 = b'''[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "toast"]
    url = file:///home/gitadmin/project1.git
    fetch = +refs/heads/*:refs/remotes/toast/*'''

sample5 = b'''[user]
  name = Your Name
  email = your.name@domain.de

[color]
  branch = auto
  diff = auto
  interactive = auto
  status = auto
[core]
  editor = vim
[merge]
  tool = vimdiff
[push]
  default = current
[pull]
[core]
  excludesfile = /home/your_name/.gitignore
[alias]
  logpretty = log --graph --decorate --pretty=oneline --abbrev-commit
  s = status'''

sample6 = b'''\xef\xbb\xbf[alias]
  s = status'''

config_files = [ full_config, with_include, sample0, sample1, sample2, sample3, sample4, sample5, sample6 ]

@pytest.fixture(params=config_files)
def config_content(request):
    return request.param

def test_simple_read_write(config_content):
    parsed = config.read_file(io.BytesIO(config_content))
    assert config.gen_file(*parsed) == config_content

def pretty_value(value):
    if value is None:
        return b''
    if value is True:
        return b'=true'
    if value is False:
        return b'=false'
    if isinstance(value, bytes):
        return b'='+value
    return ("=%s"%value).encode()

def pretty_sections(sections):
    l = []
    for section in sections:
        sectionName = section.get_full_sectionName()
        for var in section.variables:
            l.append(sectionName+b'.'+var.name+pretty_value(var.value))
    return b'\n'.join(l)

def test_compare_git(tmpdir, config_content):
    path = Path(str(tmpdir))
    _config = path.joinpath('config')
    with _config.open(mode="bw") as f:
        f.write(config_content)
    git_keys = check_output("git config -f "+str(_config)+" -l", cwd=str(path), shell=True)
    maggit_sections = config.read_file(io.BytesIO(config_content))[0]

    print(maggit_sections)
    assert git_keys == pretty_sections(maggit_sections)+b'\n'
