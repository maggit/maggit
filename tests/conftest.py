import os

def pytest_report_header(config):
    if 'MAGGIT_GIT_REF_BASE' not in os.environ:
        return ("WARNING !! MAGGIT_GIT_REF_BASE environment variable is not set\n"
                "           Some tests will not be run.")
