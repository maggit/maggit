# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit import db
import subprocess
from binascii import unhexlify, hexlify

@pytest.fixture()
def base(tmpdir):
    commands = [
        "git init",
        "echo 'foo' > file1",
        "git add file1",
        "git commit -m 'first commit'",
        "echo 'bar' >> file1",
        "git add file1",
        "git commit -m 'second commit'",
        "echo 'baz' >> file2",
        "git add file2",
        "git commit -m 'add second file'",
        "git checkout -b branch1",
        "git checkout -b branch2",
        "echo 'blablalba' > file2",
        "git add file2",
        "git commit -m 'update branch2'",
        "git checkout master",
        "echo 'new struff' > file3",
        "git add file3",
        "git commit -m 'add file to master'"
    ]
    for command in commands:
        subprocess.call(command, shell=True, cwd=str(tmpdir))
    return tmpdir


def test_repo_create(base):
    repo = db.Repo(gitdir=str(base))
    assert str(repo.gitdir) == base/'.git'

def test_branches(base):
    repo = db.Repo(gitdir=str(base))
    branches = repo.refs.branches
    assert set(branches) == set(('master', 'branch1', 'branch2'))

def test_HEAD(base):
    repo = db.Repo(gitdir=str(base))
    assert repo.refs.HEAD == 'master'

def test_full_sha(base):
    repo = db.Repo(gitdir=str(base))
    s = repo.db.get_full_sha("1c59")
    assert type(s) == bytes
    assert s == unhexlify(b"1c5996bb930163f7b022fd10a3fe9411724bf02d")
    assert repo.db.get_full_sha("4cb6") == unhexlify(b"4cb632d71608a316b8d7c5d38ffa00354ae80db1")

def test_full_sha_pack(base):
    subprocess.call(['git', 'gc'], cwd=str(base))
    repo = db.Repo(gitdir=str(base))
    s = repo.db.get_full_sha("1c59")
    assert type(s) == bytes
    assert s == unhexlify(b"1c5996bb930163f7b022fd10a3fe9411724bf02d")
    assert repo.db.get_full_sha("4cb6") == unhexlify(b"4cb632d71608a316b8d7c5d38ffa00354ae80db1")

