# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db import db
from binascii import unhexlify, hexlify
import os
import time
from pathlib import Path
from .util import GitWrapper

@pytest.fixture()
def gitwrapper_base(tmpdir):
    gitwrapper = GitWrapper(tmpdir)
    gitwrapper('init')
    return gitwrapper, db.Gitdb(gitwrapper.objectdir)

def test_maggitw_maggitr_blob(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    sha = base.blob_write(b'test')
    assert type(sha) == bytes
    assert base.blob_content(sha) == b'test'

def test_gitw_maggitr_blob(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test')
    sha = unhexlify(out)
    assert base.blob_content(sha) == b'test'

def test_maggitw_gitr_blob(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    sha = base.blob_write(b'test')
    out = gitwrapper('cat-file', '-p', hexlify(sha), raw=True)
    assert out == b'test'

def test_maggitw_maggitr_dir(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)
    assert type(treesha) == bytes

    assert base.tree_content(treesha) == {b'testfile':(b'100644', blobsha)}

def test_gitw_maggitr_dir(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin')
    blobsha = unhexlify(out)

    entries = [(b'testfile', (b'100644', blobsha))]
    content = b'100644 testfile' + bytes([0]) + blobsha
    out = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)
    treesha = unhexlify(out)

    assert base.tree_content(treesha) == {b'testfile':(b'100644', blobsha)}

def test_maggitw_gitr_dir(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)

    out = gitwrapper('cat-file', '-p', hexlify(treesha), raw=True)

    assert out == b'100644 blob '+hexlify(blobsha)+b'\ttestfile\n'

def test_maggitw_maggitr_commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)
    assert type(treesha) == bytes

    commitsha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now")
    assert type(commitsha) == bytes

    assert base.commit_content(commitsha) == (treesha, [], [b'test commit'], b'test user <test@user.com> now', b'test committer <test@committer> now')


def test_gitw_maggitr_commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test')
    blobsha = unhexlify(out)

    entries = [(b'100644', blobsha, b'testfile')]
    content = b'100644 testfile' + bytes([0]) + blobsha
    out = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)
    treesha = unhexlify(out)

    env = dict(os.environ)
    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    env['GIT_AUTHOR_NAME'] = b'test user'
    env['GIT_AUTHOR_EMAIL'] = b'test@user.com'
    env['GIT_AUTHOR_DATE'] = now
    env['GIT_COMMITTER_NAME'] = b'test committer'
    env['GIT_COMMITTER_EMAIL'] = b'test@committer.com'
    env['GIT_COMMITTER_DATE'] = now

    out = gitwrapper('commit-tree', hexlify(treesha), input=b'test commit', env=env)
    commitsha = unhexlify(out)

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    assert base.commit_content(commitsha) == (treesha, [], [b'test commit'], author, committer)


def test_maggitw_gitr_commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)

    now = bytes(str(time.time()), 'ascii')
    commitsha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now)

    out = gitwrapper('cat-file', '-p', hexlify(commitsha), raw=True)

    assert out == b'tree ' + hexlify(treesha) + b'\nauthor test user <test@user.com> ' + now + b'\ncommitter test committer <test@committer.com> ' + now + b'\n\ntest commit'


def test_maggitw_maggitr_1commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)
    assert type(treesha) == bytes

    commit0sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now")
    assert type(commit0sha) == bytes
    commit1sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now", parents=[commit0sha])
    assert type(commit1sha) == bytes

    assert base.commit_content(commit1sha) == (treesha, [commit0sha], [b'test commit'], b'test user <test@user.com> now', b'test committer <test@committer> now')


def test_gitw_maggitr_1commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test')
    blobsha = unhexlify(out)

    entries = [(b'100644', blobsha, b'testfile')]
    content = b'100644 testfile' + bytes([0]) + blobsha
    out = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)
    treesha = unhexlify(out)

    env = dict(os.environ)
    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    env['GIT_AUTHOR_NAME'] = b'test user'
    env['GIT_AUTHOR_EMAIL'] = b'test@user.com'
    env['GIT_AUTHOR_DATE'] = now
    env['GIT_COMMITTER_NAME'] = b'test committer'
    env['GIT_COMMITTER_EMAIL'] = b'test@committer.com'
    env['GIT_COMMITTER_DATE'] = now

    out = gitwrapper('commit-tree', hexlify(treesha), input=b'test commit', env=env)
    commit0sha = unhexlify(out)

    out = gitwrapper('commit-tree', hexlify(treesha), '-p', hexlify(commit0sha), input=b'test commit', env=env)
    commit1sha = unhexlify(out)

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    assert base.commit_content(commit1sha) == (treesha, [commit0sha], [b'test commit'], author, committer)


def test_maggitw_gitr_1commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)

    now = bytes(str(int(time.time())), 'ascii') + b' +0100'
    commit0sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now)
    commit1sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now, parents=[commit0sha])

    out = gitwrapper('cat-file', '-p', hexlify(commit1sha), raw=True)

    assert out == b'tree ' + hexlify(treesha) + b'\nparent '+ hexlify(commit0sha) + b'\nauthor test user <test@user.com> ' + now + b'\ncommitter test committer <test@committer.com> ' + now + b'\n\ntest commit'

def test_maggitw_maggitr_2commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)
    assert type(treesha) == bytes

    commit0sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now")
    assert type(commit0sha) == bytes
    commit1sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now", parents=[commit0sha])
    assert type(commit1sha) == bytes
    commit2sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now", parents=[commit0sha, commit1sha])
    assert type(commit2sha) == bytes

    assert base.commit_content(commit2sha) == (treesha, [commit0sha, commit1sha], [b'test commit'], b'test user <test@user.com> now', b'test committer <test@committer> now')


def test_gitw_maggitr_2commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test')
    blobsha = unhexlify(out)

    entries = [(b'100644', blobsha, b'testfile')]
    content = b'100644 testfile' + bytes([0]) + blobsha
    out = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)
    treesha = unhexlify(out)

    env = dict(os.environ)
    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    env['GIT_AUTHOR_NAME'] = b'test user'
    env['GIT_AUTHOR_EMAIL'] = b'test@user.com'
    env['GIT_AUTHOR_DATE'] = now
    env['GIT_COMMITTER_NAME'] = b'test committer'
    env['GIT_COMMITTER_EMAIL'] = b'test@committer.com'
    env['GIT_COMMITTER_DATE'] = now

    out = gitwrapper('commit-tree', hexlify(treesha), env=env, input=b'test commit')
    commit0sha = unhexlify(out)

    out = gitwrapper('commit-tree', hexlify(treesha), '-p', hexlify(commit0sha), env=env, input=b'test commit')
    commit1sha = unhexlify(out)

    out = gitwrapper('commit-tree', hexlify(treesha), '-p', hexlify(commit0sha), '-p', hexlify(commit1sha), env=env, input=b'test commit')
    commit2sha = unhexlify(out)

    author =  b'test user <test@user.com> ' + now
    committer = b'test committer <test@committer.com> ' + now
    assert base.commit_content(commit2sha) == (treesha, [commit0sha, commit1sha], [b'test commit'], author, committer)


def test_maggitw_gitr_2commit(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)

    now = bytes(str(int(time.time())), 'ascii') + b' +0100'
    commit0sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now)
    commit1sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now, parents=[commit0sha])
    commit2sha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now, parents=[commit0sha, commit1sha])

    out = gitwrapper('cat-file', '-p', hexlify(commit2sha), raw=True)

    assert out == (b'tree '+hexlify(treesha)
                 + b'\nparent '+hexlify(commit0sha)
                 + b'\nparent '+hexlify(commit1sha)
                 + b'\nauthor test user <test@user.com> '+now
                 + b'\ncommitter test committer <test@committer.com> '+now
                 + b'\n\ntest commit')

def test_maggitw_maggitr_tag(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    assert type(blobsha) == bytes
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)
    assert type(treesha) == bytes

    commitsha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", b"now", b"test committer <test@committer>", b"now")
    assert type(commitsha) == bytes

    tagsha = base.tag_write(commitsha, b'test_tag', b'test tagger <test@tagger.com>', b"now", b"test tag")
    assert type(tagsha) == bytes

    assert base.tag_content(tagsha) == (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> now', [b'test tag', b''])


def test_gitw_maggitr_tag(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test')

    entries = [(b'100644', blobsha, b'testfile')]
    content = b'100644 testfile' + bytes([0]) + unhexlify(blobsha)
    treesha = gitwrapper('hash-object', '-t', 'tree', '-w', '--stdin', input=content)

    env = dict(os.environ)
    now = int(time.time())
    now = bytes(str(now), 'ascii') + b' +0000'
    env['GIT_AUTHOR_NAME'] = b'test user'
    env['GIT_AUTHOR_EMAIL'] = b'test@user.com'
    env['GIT_AUTHOR_DATE'] = now
    env['GIT_COMMITTER_NAME'] = b'test committer'
    env['GIT_COMMITTER_EMAIL'] = b'test@committer.com'
    env['GIT_COMMITTER_DATE'] = now

    commitsha = unhexlify(gitwrapper('commit-tree', treesha, env=env, input=b'test commit'))

    env = dict(os.environ)
    env['GIT_COMMITTER_NAME'] = b'test tagger'
    env['GIT_COMMITTER_EMAIL'] = b'test@tagger.com'
    env['GIT_COMMITTER_DATE'] = now
    gitwrapper('tag', 'test_tag', '-a', '-m', 'test tag', hexlify(commitsha), env=env)
    tagsha = unhexlify(gitwrapper('show-ref', '--hash', 'test_tag'))
    
    assert base.tag_content(tagsha) ==  (commitsha, b'commit', b'test_tag', b'test tagger <test@tagger.com> '+now, [b'test tag', b''])

def test_maggitw_gitr_tag(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    blobsha = base.blob_write(b'test')
    entries = [(b'testfile', (b'100644', blobsha))]
    treesha = base.tree_write(entries)

    now = bytes(str(int(time.time())), 'ascii')
    commitsha = base.commit_write(treesha, b"test commit", b"test user <test@user.com>", now, b"test committer <test@committer.com>", now)

    tagsha = base.tag_write(commitsha, b'test_tag', b'test tagger <test@tagger.com>', now, b"test tag")

    out = gitwrapper('cat-file', '-p', hexlify(tagsha), raw=True)

    assert out == b'object ' + hexlify(commitsha) + b'\ntype commit\ntag test_tag\ntagger test tagger <test@tagger.com> ' + now + b'\n\ntest tag\n'

def test_pack_access(gitwrapper_base):
    gitwrapper, base = gitwrapper_base
    out = gitwrapper('hash-object', '-t', 'blob', '-w', '--stdin', input=b'test', raw=True)
    sha = unhexlify(out[:-1])

    assert base.blob_content(sha) == b'test'

    out = gitwrapper('pack-objects', ".git/objects/pack/pack", input=out, raw=True)

    _sha = hexlify(sha).decode()
    os.unlink(str(gitwrapper.objectdir/_sha[:2]/_sha[2:]))

    base.gen_pack_list()

    assert base.blob_content(sha) == b'test'
