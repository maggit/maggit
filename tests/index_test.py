# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db import io
from subprocess import Popen, PIPE, call
from binascii import unhexlify, hexlify
from pathlib import Path
import tempfile
import random


git_init_script = ['''git init
echo 'test content' >> test.txt
git add test.txt''',

'git commit -m "first commit"',

'''git checkout -b branch
echo 'second line' >> test.txt
git add test.txt''',

'git commit -m "second commit"',

'''git checkout master
echo 'other line' >> test.txt
git add test.txt''',

'git commit -m "third commit"',

'git merge branch',

'git add test.txt',

'git commit -m "merge stuff"',

'git update-index --assume-unchanged test.txt',

'git update-index --skip-worktree test.txt',

'''echo "linnnnneeeee" > test2.txt
git add -N test2.txt'''

]

@pytest.fixture(params=range(len(git_init_script)))
def git_index_out(tmpdir, request):
    stmpdir = Path(str(tmpdir))
    for script in git_init_script[:request.param+1]:
        for l in script.split('\n'):
            call(l, shell=True, cwd=str(tmpdir))
        p = Popen("git ls-files --stage --debug", shell=True, cwd=str(tmpdir), stdout=PIPE)
        out, _ = p.communicate()
    return stmpdir, stmpdir.joinpath('.git','index'), out

mapType = {0b1000:'100', 0b1010:'101', 0b1110:'111'}
mapPerm = {0o644: '644', 0o755: '755'}
def pretty_entry(entry):
    _strs = []
    _strs.append("%s%s %s %d\t%s"%(mapType[entry.mode[0]], mapPerm[entry.mode[1]], hexlify(entry.sha).decode(),entry.stage, entry.name.decode()))
    _strs.append("  ctime: %d:%d"%(entry.st_ctime, entry.st_ctime_ns))
    _strs.append("  mtime: %d:%d"%(entry.st_mtime, entry.st_mtime_ns))
    _strs.append("  dev: %d\tino: %d"%(entry.st_dev, entry.st_ino))
    _strs.append("  uid: %d\tgid: %d"%(entry.st_uid, entry.st_gid))

    flag = entry.stage << 12
    if entry.assume_valid: flag |= 1 << 15
    if entry.extend:
        flag |= 1 << 14
        if entry.skip_worktree: flag |= 1 << 30
        if entry.intent_to_add: flag |= 1 << 29
    _strs.append("  size: %d\tflags: %x"%(entry.st_size,flag))
    
    return b'\n'.join(bytes(s, 'ascii') for s in _strs)

def pretty_entries(entries):
    return b'\n'.join(pretty_entry(e) for e in entries) + b'\n'

def test_gitdb(git_index_out):
    gitdir, index, out = git_index_out
    indexContent = io.index.index_content(index)
    assert out == pretty_entries(indexContent[0])
