# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db import io
from subprocess import Popen, PIPE, call
from binascii import unhexlify, hexlify
from pathlib import Path
import tempfile
import random

git_init_script = '''git init
echo 'test content' >> test.txt
git add test.txt
git commit -m "first commit"
echo 'second line' >> test.txt
git add test.txt
git commit -m "second commit"
git tag -a tag1 -m "a tag"
git tag -a tag2 -m "a new tag"
mkdir directory
git mv test.txt directory
git commit -m "a move"
echo "test content" >> test2.txt
git add test2.txt
git commit -m "same file twice"
echo "test content\\nnewline\\nsecond line" > test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" >> test.txt
echo "test content\\nnewline\\nsecond line" > directory/test.txt
git add test.txt directory/test.txt
git commit -m "try to generate diff"
git tag -a tag3 -m "a new tag"
echo "bla bla bla" >> test.txt
git add test.txt
git commit -m "a new diff ?"
'''

packsToTest =[(b'--index-version=1', b'pack-offset-v1'),
              (b'--index-version=1 --delta-base-offset', b'pack-base-v1'),
              (b'--index-version=2', b'pack-offset-v2'),
              (b'--index-version=2 --delta-base-offset', b'pack-base-v2'),
              (b'--index-version=2,256', b'pack-offset-v264'),
              (b'--index-version=2,256 --delta-base-offset', b'pack-base-v264'),
             ]

@pytest.fixture()
def git_base(tmpdir):
    stmpdir = Path(str(tmpdir))
    for l in git_init_script.split('\n'):
        call(l, shell=True, cwd=str(tmpdir))
    objectList = []
    for i in stmpdir.joinpath('.git','objects').glob('[0-9a-f][0-9a-f]'):
        if not i.is_dir():
            continue
        bname = i.name
        for f in i.iterdir():
            if not f.is_file():
                continue
            objectList.append(str(bname+f.name))
    packList = []
    for options, basename in packsToTest:
        p = Popen(b'git pack-objects --no-reuse-object '+options+b' '+basename, shell=True, cwd=str(tmpdir), stdin=PIPE, stdout=PIPE)
        out, _ = p.communicate(b'\n'.join(s.encode() for s in objectList))
        out = basename+b'-'+out[:-1]

        packList.append(out)

    return stmpdir, objectList, packList

@pytest.fixture(params=range(len(packsToTest)))
def packindex(request):
    return request.param

_cache = {}
def get_index_pack(gitdir, name):
    try:
        return _cache[(gitdir, name)]
    except KeyError:
        print("generating", gitdir, name)
        
        _cache[(gitdir, name)] = gitIndex, pack
        return gitIndex, pack

def test_gitdb(git_base, packindex):
    gitdir, objectList, packList = git_base
    packName = packList[packindex]
    gitIndex = io.GitPackIndex(gitdir.joinpath(packName.decode()+'.idx'))
    pack = io.GitPack(gitdir.joinpath(packName.decode()+'.pack'), gitIndex)
    for sha in objectList:
        loosepath = gitdir.joinpath('.git', 'objects', sha[:2], sha[2:])
        loose_obj = io.loose.object_content(loosepath)
        sha = unhexlify(sha.encode())
        pack_obj = pack.read_object(gitIndex.get_offset(sha))
        assert loose_obj == pack_obj
