# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db import db
from subprocess import Popen, PIPE, call
from binascii import unhexlify, hexlify
from pathlib import Path
import time

git_init_script = '''git init
echo 'test content' >> test.txt
git add test.txt
git commit -m "first commit"
echo 'second line' >> test.txt
git add test.txt
git commit -m "second commit"
git tag -a tag -m "a tag"
mkdir directory
git mv test.txt directory
git commit -m "a move"
'''

@pytest.fixture()
def git_base(tmpdir):
    stmpdir = Path(str(tmpdir))
    for l in git_init_script.split('\n'):
        call(l, shell=True, cwd=str(tmpdir))
    return stmpdir, db.Gitdb(stmpdir.joinpath('.git','objects'))


def pretty_a_tree_entry(name_, mode_sha):
    mode_, sha_ = mode_sha
    if mode_ == b'40000':
        mode_ = b'040000'
        type_ = b'tree'
    else:
        type_ = b'blob'
    return mode_+b' '+type_+b' '+hexlify(sha_)+b'\t'+name_
    
def pretty_tree_content(entries):
    return b'\n'.join(pretty_a_tree_entry(n,v) for n, v in entries.items()) + b'\n'

def pretty_commit_content(tree, parents, messagelines, author, committer):
    tree = b'tree ' + hexlify(tree)
    parents = [b'parent '+hexlify(p) for p in parents]
    author = b'author ' + author
    committer = b'committer ' + committer
    return b'\n'.join([tree]+parents+[author, committer, b'', b'\n'.join(messagelines)])

def pretty_tag_content(object_, objtype, tag, tagger, messagelines):
    object_ = b'object ' + hexlify(object_)
    objtype = b'type ' + objtype
    tag     = b'tag ' + tag
    tagger  = b'tagger ' + tagger
    return b'\n'.join([object_, objtype, tag, tagger, b'', b'\n'.join(messagelines)])

def test_gitdb(git_base):
    gitdir, base = git_base
    for i in gitdir.joinpath('.git','objects').glob('[0-9a-f][0-9a-f]'):
        if not i.is_dir():
            continue
        bname = i.name
        for f in i.iterdir():
            if not f.is_file():
                continue
            # ensure type is good
            git_sha = unhexlify(str(bname+f.name).encode())
            p = Popen(b'git cat-file -t '+hexlify(git_sha), shell=True, stdout=PIPE, cwd=str(gitdir))
            out, _ = p.communicate()
            out = out[:-1]
            assert out == bytes(base.object_type(git_sha))

            p = Popen(b'git cat-file -p '+hexlify(git_sha), shell=True, stdout=PIPE, cwd=str(gitdir))
            prettyout, _ = p.communicate()
            print(prettyout)
            if out == b'blob':
                assert prettyout == base.blob_content(git_sha)
            if out == b'tree':
                assert prettyout == pretty_tree_content(base.tree_content(git_sha))
            if out == b'commit':
                assert prettyout == pretty_commit_content(*base.commit_content(git_sha))
            if out == b'tag':
                assert prettyout == pretty_tag_content(*base.tag_content(git_sha))
        
