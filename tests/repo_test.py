# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import os
import shutil

import pytest

from maggit.repo import Repo, Entry
from maggit.sha import Sha
from maggit import refs, gitObjects, Person, Blob
from subprocess import call, check_output
from binascii import unhexlify, hexlify
import datetime

@pytest.fixture(params=[(False, False), (False, True), (True, False), (True, True)])
def repo_base(tmpdir, request):
    commands = [
        "git init",
        "git config user.name 'Git Tester'",
        "git config user.email 'git@tester.com'",
        "echo 'foo' > file1",
        "git add file1",
        "git commit -m 'first commit'",
        "echo 'bar' >> file1",
        "git add file1",
        "git commit -m 'second commit'",
        "git tag -m 'tag master' tag1",
        "echo 'baz' >> file2",
        "git add file2",
        "git commit -m 'add second file'",
        "git checkout -b branch1",
        "git checkout -b branch2",
        "echo 'blablalba' > file2",
        "git add file2",
        "git commit -m 'update branch2'",
        "git tag -m 'tag branch2' tag2",
        "git checkout master",
        "echo 'new struff' > file3",
        "git add file3",
        "git commit -m 'add file to master'"
    ]
    relative, packed = request.param
    if packed:
        commands.append("git gc")
    for command in commands:
        call(command, shell=True, cwd=str(tmpdir))
    root = str(tmpdir)
    if relative:
        parent = os.path.dirname(root)
        currentdir = os.curdir
        os.chdir(parent)
        def fin():
            os.chdir(currentdir)
        request.addfinalizer(fin)
        root = os.path.basename(root)
    return Repo(gitdir=root), str(tmpdir)

def test_branches(repo_base):
    repo, base = repo_base
    branches = repo.branches
    for b in ('master', 'branch1', 'branch2'):
        out = check_output(["git", "rev-parse", b], cwd=base, universal_newlines=True)
        out = unhexlify(out[:-1])
        assert type(branches[b].sha) == Sha
        assert branches[b].sha == out

def test_commit(repo_base):
    repo, base = repo_base
    branches = repo.branches
    for b in ('master', 'branch1', 'branch2'):
        commit = branches[b].commit
        assert type(commit.sha) == Sha
        assert commit.sha == branches[b].sha
        assert commit.author == Person(b'Git Tester', b'git@tester.com')
        assert commit.committer == Person(b'Git Tester', b'git@tester.com')
        assert type(commit.author_date) == datetime.datetime
        assert type(commit.committer_date) == datetime.datetime

def test_tag(repo_base):
    repo, base = repo_base
    tags = repo.tags
    for b in ('tag1', 'tag2'):
        assert type(tags[b]) == refs.Tag
        assert type(tags[b].object) == gitObjects.Tag
        assert type(tags[b].commit) == gitObjects.Commit
        assert tags[b].object.object == tags[b].commit
        assert tags[b].object.object is tags[b].commit

def test_entry(repo_base):
    repo, base = repo_base
    master = repo.branches['master']
    entry = Entry(master.commit, b'file1')
    assert entry.commit == master.commit
    assert entry.gitObject.sha == repo.get_full_sha("3bd1f0")
    assert type(entry.gitObject.sha) == Sha
    assert type(repo.get_full_sha("3bd1f0")) == Sha
    assert entry.path == b'file1'

def test_first_appearance(repo_base):
    repo, base = repo_base
    entry = Entry(repo.branches['master'].commit, b'file1')
    first_entry_commit = entry.get_first_appearance()
    assert first_entry_commit.message == "second commit\n"

def test_first_appearances_head(repo_base):
    repo, base = repo_base
    dict_ = repo.HEAD.commit.get_first_appearances()
    dict_ = {k:c.message for k,c in dict_.items()}
    assert dict_ == { b'file1' : 'second commit\n',
                      b'file2' : 'add second file\n',
                      b'file3' : 'add file to master\n'}

def test_first_appearances_branch1(repo_base):
    repo, base = repo_base
    dict_ = repo.branches['branch1'].commit.get_first_appearances()
    dict_ = {k:c.message for k,c in dict_.items()}
    assert dict_ == { b'file1' : 'second commit\n',
                      b'file2' : 'add second file\n'}

def test_first_appearances_branch2(repo_base):
    repo, base = repo_base
    dict_ = repo.branches['branch2'].commit.get_first_appearances()
    dict_ = {k:c.message for k,c in dict_.items()}
    assert dict_ == { b'file1' : 'second commit\n',
                      b'file2' : 'update branch2\n'}

def test_init_repo(tmpdir):
    gitpath = str(tmpdir)
    repo = Repo.init_repo(gitpath)

    assert os.path.exists(gitpath)
    assert os.listdir(gitpath) == ['.git']
    assert os.path.exists(os.path.join(gitpath, '.git'))
    assert sorted(os.listdir(os.path.join(gitpath, '.git'))) == sorted([
        'config', 'objects', 'branches', 'description', 'HEAD', 'refs',
        'hooks', 'info'])
    assert sorted(os.listdir(
        os.path.join(gitpath, '.git', 'objects'))) == ['info', 'pack']
    assert sorted(os.listdir(
        os.path.join(gitpath, '.git', 'refs'))) == ['heads', 'tags']

    assert list(repo.branches) == []
    assert not repo.bare


def test_init_repo_bare(tmpdir):
    gitpath = str(tmpdir)
    repo = Repo.init_repo(gitpath, bare=True)

    assert os.path.exists(gitpath)
    assert sorted(os.listdir(gitpath)) == sorted([
        'config', 'objects', 'branches', 'description', 'HEAD', 'refs',
        'hooks', 'info'])
    assert sorted(os.listdir(
        os.path.join(gitpath, 'objects'))) == ['info', 'pack']
    assert sorted(os.listdir(
        os.path.join(gitpath, 'refs'))) == ['heads', 'tags']

    assert list(repo.branches) == []
    assert repo.bare

def test_direct_object_acces(repo_base):
    repo, base = repo_base
    Blob(repo, b"1c5996bb930163f7b022fd10a3fe9411724bf02d")
    Blob(repo, "1c5996bb930163f7b022fd10a3fe9411724bf02d")
    Blob(repo, unhexlify(b"1c5996bb930163f7b022fd10a3fe9411724bf02d"))
    Blob(repo, unhexlify("1c5996bb930163f7b022fd10a3fe9411724bf02d"))
