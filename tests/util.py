# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

from subprocess import Popen, PIPE, call
from pathlib import Path


class GitWrapper:
    def __init__(self, cwd):
        self.cwd = str(cwd)
    
    def __call__(self, *args, **kwords):
        stdin = kwords.get('input', '')
        call_kwords = { 'stdin': PIPE,
                        'stdout': PIPE,
                        'cwd': self.cwd}
        if 'env' in kwords:
            call_kwords['env'] = kwords['env']
        p = Popen(['git'] + list(args), **call_kwords)
        stdout, stderr = p.communicate(stdin)
        if kwords.get('raw', False):
            return stdout
        return stdout[:-1]

    @property
    def objectdir(self):
        return Path(self.cwd)/'.git'/'objects'
