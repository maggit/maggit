# This file is part of maggit.
#
# Copyright 2015 Matthieu Gautier <dev@mgautier.fr>
#
# Pit is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# Additional permission under the GNU Affero GPL version 3 section 7:
#
# If you modify this Program, or any covered work, by linking or
# combining it with other code, such other code is not for that reason
# alone subject to any of the requirements of the GNU Affero GPL
# version 3.
#
# You should have received a copy of the GNU Affero General Public License
# along with maggit.  If not, see http://www.gnu.org/licenses
#
# In summary:
# - You can use this program for no cost.
# - You can use this program for both personal and commercial reasons.
# - You do not have to share your own program's code which uses this program.
# - You have to share modifications (e.g bug-fixes, improvements) you've made to this program.

import pytest

from maggit.db.io import config
import io
from subprocess import check_output, call
from pathlib import Path

startConfigEmpty = b''
startConfigNewLine = b'\n'
startConfigComment = b'# With a comment\n'
startConfigSectionExisting0 = b'[core]\n\tfoo=bar'
startConfigSectionExisting1 = b'[core]\n\tfoo=bar\n'
startConfigSectionExisting2 = b'[core]\n\tfoo=bar;comment'
startConfigSectionExisting3 = b'[core]\n\tfoo=bar;comment\n'
startConfigSectionExisting4 = b'[core]\n\tfoo=bar;comment\n#another comment'
startConfigSectionExisting5 = b'[core]\n\tfoo=bar;comment\n#another comment\n'
startConfigCommentVar = b'\n# With a comment\n[core]\n  #this is a variable\n  name = value\n'


        
        

full_config = b"""#
# This is the config file, and
# a '#' or ';' character indicates
# a comment
#

; core variables
[core]
       ; Don't trust file modes
       filemode = false

; Our diff algorithm
[diff]
       external = /usr/local/bin/diff-wrapper
       renames = true

; Proxy settings
[core]
       gitproxy=proxy-command for kernel.org
       gitproxy=default-proxy ; for all the rest

; HTTP
[http]
       sslVerify
[http "https://weak.example.com"]
       sslVerify = false
       cookieFile = /tmp/cookie.txt"""

with_include = b"""# Core variables
[core]
       ; Don't trust file modes
       filemode = false

# Our diff algorithm
[diff]
       external = /usr/local/bin/diff-wrapper
       renames = true

[branch "devel"]
       remote = origin
       merge = refs/heads/devel

# Proxy settings
[core]
       gitProxy="ssh" for "kernel.org"
       gitProxy=default-proxy ; for the rest

[include]
       path = /path/to/foo.inc ; include by absolute path
       path = foo ; expand "foo" relative to the current file
       path = ~/foo ; expand "foo" in your $HOME directory"""

sample0 = br'''[user]
	name = Pavan Kumar Sunkara
	email = pavan.sss1991@gmail.com
[core]
	editor = vim
	whitespace = fix,-indent-with-non-tab,trailing-space,cr-at-eol
	excludesfile = ~/.gitignore
[sendemail]
	smtpencryption = tls
	smtpserver = smtp.gmail.com
	smtpuser = pavan.sss1991@gmail.com
	smtppass = password
	smtpserverport = 587
[web]
	browser = google-chrome
[instaweb]
	httpd = apache2 -f
[rerere]
	enabled = 1
	autoupdate = 1
[push]
	default = matching
[color]
	ui = auto
[color "branch"]
	current = yellow bold
	local = green bold
	remote = cyan bold
[color "diff"]
	meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
	whitespace = red reverse
[color "status"]
	added = green bold
	changed = yellow bold
	untracked = red bold
[diff]
	tool = vimdiff
[difftool]
	prompt = false
[github]
	user = pkumar
	token = token
[gitflow "prefix"]
	feature = feature-
	release = release-
	hotfix = hotfix-
	support = support-
	versiontag = v
[alias]
	a = add --all
	ai = add -i
	#############
	ap = apply
	as = apply --stat
	ac = apply --check
	#############
	ama = am --abort
	amr = am --resolved
	ams = am --skip
	#############
	b = branch
	ba = branch -a
	bd = branch -d
	br = branch -r
	#############
	c = commit
	ca = commit -a
	cm = commit -m
	cem = commit --allow-empty -m
	cam = commit -am
	cd = commit --amend
	cad = commit -a --amend
	ced = commit --allow-empty --amend
	#############
	d = diff
	dc = diff --cached
	dl = difftool
	dlc = difftool --cached
	dk = diff --check
	dp = diff --patience
	dck = diff --cached --check
	#############
	f = fetch
	fo = fetch origin
	fu = fetch upstream
	#############
	fp = format-patch
	#############
	fk = fsck
	#############
	g = grep -p
	#############
	l = log --oneline
	lg = log --oneline --graph --decorate
	#############
	ls = ls-files
	lsf = "!git ls-files | grep -i"
	#############
	m = merge
	ma = merge --abort
	mc = merge --continue
	ms = merge --skip
	#############
	o = checkout
	ob = checkout -b
	#############
	pr = prune -v
	#############
	ps = push
	psf = push -f
	psu = push -u
	pso = push origin
	psao = push --all origin
	psfo = push -f origin
	psuo = push -u origin
	psom = push origin master
	psfom = push -f origin master
	psuom = push -u origin master
	#############
	pl = pull
	plu = pull -u
	plo = pull origin
	plp = pull upstream
	plom = pull origin master
	plpm = pull upstream master
	#############
	pb = pull --rebase
	pbo = pull --rebase origin
	pbp = pull --rebase upstream
	pbom = pull --rebase origin master
	pbpm = pull --rebase upstream master
	#############
	rb = rebase
	rba = rebase --abort
	rbc = rebase --continue
	rbi = rebase --interactive
	rbs = rebase --skip
	#############
	re = reset
	rh = reset HEAD
	reh = reset --hard
	rem = reset --mixed
	res = reset --soft
	rehh = reset --hard HEAD
	remh = reset --mixed HEAD
	resh = reset --soft HEAD
	#############
	r = remote
	ra = remote add
	rr = remote rm
	rv = remote -v
	rm = remote rename
	rp = remote prune
	rs = remote show
	rao = remote add origin
	rau = remote add upstream
	rso = remote show origin
	rsu = remote show upstream
	rpo = remote prune origin
	rpu = remote prune upstream
	#############
	s = status
	sb = status -s -b
	#############
	sa = stash apply
	sc = stash clear
	sd = stash drop
	sl = stash list
	sp = stash pop
	ss = stash save
	sw = stash show
	#############
	w = show
	wp = show -p
	wr = show -p --no-color
	#############
	svnr = svn rebase
	svnd = svn dcommit
	svnl = svn log --oneline --show-commit
	#############
	assume = update-index --assume-unchanged
	unassume = update-index --no-assume-unchanged
	assumed = "!git ls-files -v | grep ^h | cut -c 3-"
	unassumeall = !git assumed | xargs git update-index --no-assume-unchanged
	assumeall = "!git st -s | awk {'print $2'} | xargs git assume"
	#############
	ours = "!f() { git checkout --ours $@ && git add $@; }; f"
	theirs = "!f() { git checkout --theirs $@ && git add $@; }; f"
	#############
	whois = "!sh -c 'git log -i -1 --pretty=\"format:%an <%ae>\n\" --author=\"$1\"' -"
	whatis = show -s --pretty='tformat:%h (%s, %ad)' --date=short
	#############
	barebranch = !sh -c 'git symbolic-ref HEAD refs/heads/$1 && git rm --cached -r . && git clean -xfd' -
	flat = clone --depth 1
	subpull = !git submodule foreach git pull --tags origin master
	subrepo = !sh -c 'filter-branch --prune-empty --subdirectory-filter $1 master' -
	human = name-rev --name-only --refs=refs/heads/*
	serve = !git daemon --reuseaddr --verbose  --base-path=. --export-all ./.git
	snapshot = !git stash save "snapshot: $(date)" && git stash apply "stash@{0}"'''

sample1 = b'''[user]
	name = Pavan Kumar Sunkara
	email = pavan.sss1991@gmail.com
[core]
	editor = vim
	whitespace = fix,-indent-with-non-tab,trailing-space,cr-at-eol
[sendemail]
	smtpencryption = tls
	smtpserver = smtp.gmail.com
	smtpuser = pavan.sss1991@gmail.com
	smtppass = password
	smtpserverport = 587
	to = git@vger.kernel.org
[web]
	browser = google-chrome
[rerere]
	enabled = 1
	autoupdate = 1
[alias]
	s = status
	sb = status -s -b
	#############
	sa = stash apply
	sc = stash clear
	sd = stash drop
	sl = stash list
	sp = stash pop
	ss = stash save
	#############
	d = diff
	dc = diff --cached
	dk = diff --check
	dck = diff --cached --check
	#############
	c = commit
	ca = commit -a
	cm = commit -m
	cam = commit -am
	cd = commit --amend
	cad = commit -a --amend
	#############
	a = add .
	#############
	ap = apply
	as = apply --stat
	ac = apply --check
	#############
	l = log --oneline
	lg = log --oneline --graph --decorate
	#############
	o = checkout
	ob = checkout -b
	#############
	b = branch
	ba = branch -a
	bd = branch -d
	br = branch -r
	#############
	f = fetch
	fo = fetch origin
	#############
	fp = format-patch
	#############
	fk = fsck
	#############
	m = merge
	#############
	pr = prune -v
	#############
	ps = push
	pl = pull
	pb = pull --rebase
	psf = push -f
	psu = push -u
	plu = pull -u
	pso = push origin
	plo = pull origin
	pbo = pull --rebase origin
	psfo = push -f origin
	psuo = push -u origin
	pluo = pull -u origin
	#############
	rb = rebase
	#############
	re = reset
	rh = reset HEAD
	reh = reset --hard
	rem = reset --mixed
	res = reset --soft
	rehh = reset --hard HEAD
	remh = reset --mixed HEAD
	resh = reset --soft HEAD
	#############
	r = remote
	ra = remote add
	rm = remote rm
	rv = remote -v
	rp = remote prune
	rs = remote show
	rso = remote show origin
	rpo = remote prune origin
	#############
	w = show
	#############
	meldon = config diff.external /home/pkumar/.meld.py
	meldof = config --remove-section diff
	#############
	human = name-rev --name-only --refs=refs/heads/*
[color]
	ui = true
[color "branch"]
	current = yellow bold
	local = green bold
	remote = cyan bold
[color "diff"]
	meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
	whitespace = red reverse
[color "status"]
	added = green bold
	changed = yellow bold
	untracked = red bold
[github]
	user = pkumar
	token = token'''


sample2 = b'''[user] 
name = John Smith
email = john@example.com
[alias]
st = status
co = checkout
br = branch
up = rebase
ci = commit
[core]
editor = vim'''

sample3 = b'''[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "origin"]
    url = git://repohost/project1.git
    fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
    remote = origin
    merge = refs/heads/master'''

sample4 = b'''[core]
    repositoryformatversion = 0
    filemode = true
    bare = false
    logallrefupdates = true
[remote "toast"]
    url = file:///home/gitadmin/project1.git
    fetch = +refs/heads/*:refs/remotes/toast/*'''

sample5 = b'''[user]
  name = Your Name
  email = your.name@domain.de

[color]
  branch = auto
  diff = auto
  interactive = auto
  status = auto
[core]
  editor = vim
[merge]
  tool = vimdiff
[push]
  default = current
[pull]
[core]
  excludesfile = /home/your_name/.gitignore
[alias]
  logpretty = log --graph --decorate --pretty=oneline --abbrev-commit
  s = status'''

sample6 = b'''[alias]
  s = status'''

startConfigs = (startConfigEmpty, startConfigNewLine, startConfigComment, startConfigSectionExisting0, startConfigSectionExisting1, startConfigSectionExisting2, startConfigSectionExisting3, startConfigSectionExisting4, startConfigSectionExisting5, startConfigCommentVar, full_config, with_include, sample0, sample1, sample2, sample3, sample4, sample5, sample6)
@pytest.fixture(params=startConfigs)
def listConfig(request):
    return request.param

@pytest.fixture(params=(b'\xef\xbb\xbf', b''))
def startConfig(listConfig, request):
    return request.param+listConfig

actions = [ ('set', (b'core.name', b'value1')) ]
@pytest.fixture(params=actions)
def action(request):
    return request.param


def test_action(startConfig, action, tmpdir):
    tmpdir = Path(str(tmpdir))
    git_config = tmpdir.joinpath('git_config')
    with git_config.open("bw") as f:
        f.write(startConfig)
    
    maggit_config = tmpdir.joinpath('maggit_config')
    maggit_sections, end, wBOM = config.read_file(io.BytesIO(startConfig))

    action, what = action
    if action == 'set':
        variable, value = what
        call(b"git config -f "+str(git_config).encode()+b" "+variable+b" "+value, cwd=str(tmpdir), shell=True)
        sectionName, name = variable.rsplit(b'.', maxsplit=1)
        section = None
        found = False
        for _section in maggit_sections:
            print(_section.get_full_sectionName(), sectionName)
            if _section.get_full_sectionName() == sectionName:
                section = _section
                for var in _section.variables:
                    print(var.name, name)
                    if var.name == name:
                        var.set_value(value)
                        found = True
                        break
                if found:
                    break
        else:
            if section is None:
                sName, *sSubName = sectionName.split(b'.', maxsplit=1)
                sSubName = sSubName or None
                section = config.Section(sName, sSubName)
                if not maggit_sections:
                    # we are alone => take any content in the text before us
                    if wBOM and not end:
                        end = b'\n'
                    section.update_leading_space(end)
                    end = b''
                maggit_sections.append(section)
                
            var = config.Variable(name, value)
            if section is maggit_sections[-1] and not end:
                # we are the last => force a \n after us
                end = b'\n'
            section.add_variable(var)

    with maggit_config.open("bw") as f:
        f.write(config.gen_file(maggit_sections, end, wBOM))

    print(maggit_sections)
    with git_config.open("br") as f:
        git_content = f.read()
    with maggit_config.open("br") as f:
        maggit_content = f.read()
    assert git_content == maggit_content
